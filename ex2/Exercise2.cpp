/*
 *      We will use this template for all OpenGL examples in ICP3036
 *
 *      This file must contain your code for:
 *           void drawScene(GLvoid);
 *           void resize(int w, int h);
 *           void setup(void);
 *           void keyInput(unsigned char key, int x, int y);
 */
 
#include "WindowingSystem.h"                // Header File for WindowingSystem.cpp

using namespace std;

/* This is where you put all your OpenGL drawing commands */
void drawScene(void)									// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
	glLoadIdentity();
    				// Reset The Current Modelview Matrix
   double r = 1;
   double g = 1;
   double b = 1;
   int startx = 0;
    glPointSize(5.0f);
    glBegin(GL_POINTS);
  for(int i = 0;i<64;i++)
  {
      r -= .09;
      g -= .12;
      b -= .15;
          
      if (r < 0.15) r = 1.0f;
      if (g < 0.15) g = 1.0f;
      if (b < 0.15) b = 1.0f;  
    glColor3d(r,g,b);    
    glVertex2i(startx+=10,240);    
    }
    glEnd();

    glutSwapBuffers();      //swaps the front and back buffers
}

/* Initialisation routine - acts like your typical constructor in a Java program. */
void setup(void)										// All Setup For OpenGL Goes Here
{
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// Type of depth testing to do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
}

/* Tells the program how to resize your OpenGL Window */
void resize(int width, int height)
{
       
	if (height==0)										// Prevent a divide by zero by
	{										            // making height equal one
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);		// Set viewport size to be entire OpenGL window

	glMatrixMode(GL_PROJECTION);						// Select the Projection Matrix
	glLoadIdentity();									// Reset the Projection Matrix

	// Calculate The Aspect Ratio Of The Window
    gluOrtho2D(0.0, width, 0.0, height);

	glMatrixMode(GL_MODELVIEW);							// Select the Modelview Matrix
	glLoadIdentity();									// Reset the Modelview Matrix
}

/* Keyboard input processing routine */
void keyInput(unsigned char key, int x, int y)
{
   switch(key) 
   {
	  // Press escape to exit.
      case 27:
         exit(0);
         break;
      default:
         break;
   }
}
