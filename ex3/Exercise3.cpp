/*
 *      We will use this template for all OpenGL examples in ICP3036
 *
 *      This file must contain your code for:
 *           void drawScene(GLvoid);
 *           void resize(int w, int h);
 *           void setup(void);
 *           void keyInput(unsigned char key, int x, int y);
 */
 
#include "WindowingSystem.h"                // Header File for WindowingSystem.cpp

using namespace std;

/* This is where you put all your OpenGL drawing commands */
void drawScene(void)									// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
	glLoadIdentity();									// Reset The Current Modelview Matrix
 
    float red = 1.0f;
    float green = 0.0f;
    float blue = 0.5f;  
        
    glColor3f(red, green, blue);   
    glBegin(GL_QUADS); 
        
      //1st Plane
      glVertex3i(0, 30, 30);
      glVertex3i(10, 30, 30);
      glVertex3i(10, 20, 30);
      glVertex3i(0, 20, 30);
    
      //2nd Plane
      glVertex3i(20, 20, 37);
      glVertex3i(30, 20, 37);
      glVertex3i(30, 10, 37);
      glVertex3i(20, 10, 37);
    
      //3rd Plane
      glVertex3i(40, 10, 45);
      glVertex3i(50, 10, 45);
      glVertex3i(50, 0, 45);
      glVertex3i(40, 0, 45);
      
    glEnd();
    glFlush();




    glutSwapBuffers();      //swaps the front and back buffers
}

/* Initialisation routine - acts like your typical constructor in a Java program. */
void setup(void)										// All Setup For OpenGL Goes Here
{
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// Type of depth testing to do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
}

/* Tells the program how to resize your OpenGL Window */
void resize(int width, int height)
{
       
	if (height==0)										// Prevent a divide by zero by
	{										            // making height equal one
		height=1;
	}
  

	glViewport(0,0,(GLsizei)width,(GLsizei)height);		// Set viewport size to be entire OpenGL window
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-30.0, 70.0, -20.0, 40.0, 0.0, -60.0);
       glMatrixMode(GL_MODELVIEW);
       gluLookAt( 25, 15,  0, // eye
                  25, 15, 30, // at
                  0,  1,  0   // up
                  );
        glLoadIdentity();
       
           					// Reset the Projection Matrix

	// Calculate The Aspect Ratio Of The Window

   								// Reset the Modelview Matrix
}

/* Keyboard input processing routine */
void keyInput(unsigned char key, int x, int y)
{
   switch(key) 
   {
	  // Press escape to exit.
      case 27:
         exit(0);
         break;
      default:
         break;
   }
}
